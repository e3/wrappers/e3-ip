# This is required for now because at the moment, ip uses the deprecated
# `struct rset` instead of the newer `struct typed_rset`. See for example
# https://github.com/epics-base/epics-base/blob/42604fc/modules/database/src/ioc/dbStatic/recSup.h#L58
# and https://gitlab.esss.lu.se/e3/e3-require/-/blob/6ddaed0/require-ess/tools/driver.makefile#L194-L196
LEGACY_RSET = YES

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



APP:=ipApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

USR_INCLUDES += -I$(where_am_I)$(APPSRC)


USR_CFLAGS   += -Wno-unused-variable
USR_CFLAGS   += -Wno-unused-function
USR_CFLAGS   += -Wno-unused-but-set-variable
USR_CPPFLAGS += -Wno-unused-variable
USR_CPPFLAGS += -Wno-unused-function
USR_CPPFLAGS += -Wno-unused-but-set-variable


SOURCES += $(APPSRC)/devXxStrParm.c
SOURCES += $(APPSRC)/devXxEurotherm.c
SOURCES += $(APPSRC)/devAiHeidND261.c
SOURCES += $(APPSRC)/devAiMKS.c
SOURCES += $(APPSRC)/Keithley2kDMM.st
SOURCES += $(APPSRC)/Keithley65xxEM.st
SOURCES += $(APPSRC)/Keithley2kDMM_mf40.st
SOURCES += $(APPSRC)/Federal.st
SOURCES += $(APPSRC)/Oxford_CS800.st
SOURCES += $(APPSRC)/devMPC.c
SOURCES += $(APPSRC)/devGP307gpib.c
SOURCES += $(APPSRC)/devXxHeidenhainGpib.c
SOURCES += $(APPSRC)/devXxAX301.c
SOURCES += $(APPSRC)/devXxKeithleyDMM199Gpib.c
# SOURCES_vxWorks += $(APPSRC)/tyGSAsynInit.c
SOURCES += $(APPSRC)/devTelevac.c
SOURCES += $(APPSRC)/devTPG261.c

DBDS    += $(APPSRC)/ipSupport.dbd
# libip.so: undefined symbol: tyGSAsynInitBuffsize
# we excluded tyGSAsynInit.c because it will be needed
# for vxWorks, so I also exclude ipVXSupport, because
# driver.makefile combine all dbds to make one ip.dbd

# DBDS    += $(APPSRC)/ipVXSupport.dbd



TEMPLATES += $(wildcard $(APPDB)/*.db)
# ip has *.proto and *.protocol files in $(APPDB)
TEMPLATES += $(wildcard $(APPDB)/*.proto*)


.PHONY: vlibs
vlibs:
